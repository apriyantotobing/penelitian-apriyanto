package com.example.wbangateway.Heartrate;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface Heartratedao {

    @Insert
    void insert(Heartrate heartrate);

    @Query("SELECT * FROM Heartrate")
    LiveData<List<Heartrate>> getLive();

    @Query("SELECT * FROM Heartrate")
    List<Heartrate> get();

    @Query("DELETE FROM Heartrate")
    void delete();

    @Query("SELECT COUNT(id) FROM Heartrate")
    int count();

//    @Query("SELECT * FROM")
//    LiveData<List<Product>> getAllProducts();

}
