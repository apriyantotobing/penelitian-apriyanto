package com.example.wbangateway;

import android.app.Application;
import android.app.NotificationManager;
import android.os.Build;

public class NotificationServiceChannel extends Application {
    public static final String CHANNEL_ID = "exampleServiceChannel";
    public static final String CHANNEL_ID2 = "exampleServiceChannel2";

    @Override
    public void onCreate() {
        super.onCreate();

        createNotificationChannel();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            android.app.NotificationChannel serviceChannel = new android.app.NotificationChannel(
                    CHANNEL_ID,
                    "Example Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            android.app.NotificationChannel serviceChannel2 = new android.app.NotificationChannel(
                    CHANNEL_ID2,
                    "Example Service Channel 2",
                    NotificationManager.IMPORTANCE_HIGH
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
            manager.createNotificationChannel(serviceChannel2);
        }
    }
}