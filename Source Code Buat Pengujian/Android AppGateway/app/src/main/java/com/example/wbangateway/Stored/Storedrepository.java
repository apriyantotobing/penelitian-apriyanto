package com.example.wbangateway.Stored;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

public class Storedrepository {

    private String DB_NAME = "db_task2";

    private Storeddb storedDatabase;

    public Storedrepository(Context context) {
        storedDatabase = Room.databaseBuilder(context, Storeddb.class, DB_NAME).allowMainThreadQueries().fallbackToDestructiveMigration().build();
    }

    public void insert(final Stored stored) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                storedDatabase.daoAccess().insert(stored);
                return null;
            }
        }.execute();
    }

    public void delete() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                storedDatabase.daoAccess().delete();
                return null;
            }
        }.execute();
    }

    public LiveData<List<Stored>> getLive() {
        return storedDatabase.daoAccess().getLive();
    }

    public List<Stored> get(){
        return storedDatabase.daoAccess().get();
    }
}