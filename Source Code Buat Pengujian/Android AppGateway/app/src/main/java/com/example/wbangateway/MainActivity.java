package com.example.wbangateway;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wbangateway.Stored.Stored;
import com.example.wbangateway.Stored.Storedrepository;
import com.google.gson.Gson;
import com.instacart.library.truetime.TrueTime;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import static android.bluetooth.BluetoothAdapter.STATE_CONNECTED;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivityTAG";

    MqttHelper mqttHelper;
    private boolean mqttConnectStatus;

    private TextView textHeartRate, textStepCounter, textAcceleration, textGyroscope, textStatus;

    private Handler mhandler;

    private BluetoothAdapter bluetoothAdapter;
    private int REQUEST_ENABLE_BT = 1;
    public BluetoothGatt gatt;
    private BluetoothDevice myDevice;
    private String MacAddress = "98:28:A6:D3:30:EE";

    private int messageSize = 1; // 200
    List<heartrate> y = new ArrayList<>();

    // variabel ketika menerima data
    private int stepcounter;
    private float heartrate, acc_x, acc_y, acc_z, gyro_x, gyro_y, gyro_z;
    long date_diff;

    int countDataTemp = 0;
    int index0 = 0;
    int index1 = messageSize;
    int a = 1;

    Timer timer;
    TimerTask timerTask;
    Gson gson;

    private Storedrepository storedrepository;
    private List<Stored> allStored;
    private List<heartrate> hrmdata = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        storedrepository = new Storedrepository(getApplicationContext());
        storedrepository.delete();

        textHeartRate = findViewById(R.id.textView1);
        textStepCounter = findViewById(R.id.textView2);
        textAcceleration = findViewById(R.id.textView3);
        textGyroscope = findViewById(R.id.textView4);
        textStatus = findViewById(R.id.textView5);

        textHeartRate.setText("getting BPM data");
        textStepCounter.setText("getting Step data");
        textAcceleration.setText("getting Acceleration data");
        textAcceleration.setText("getting Gyroscope data");
        textStatus.setText("connecting to MQTT Broker");
        Button button = (Button) findViewById(R.id.changeMessageSize);
        gson = new Gson();

        /* inisialisasi waktu NTP Server
        harus dalam sebuah background thread */
        Thread time = new Thread() {
            public void run() {
                String date;
                SimpleDateFormat dateFormat = new SimpleDateFormat(
                        "yyyy-MM-dd HH:mm:ss.SSS");
                try {
                    TrueTime.build().initialize();
                    date = dateFormat.format(TrueTime.now());
                    Log.d(TAG, "DATA WAKTU: " + date);

                    Date date2 = dateFormat.parse(date);
                    date2.getTime();

                    Date date3 = new Date();
                    date3.getTime();
                    date_diff = date2.getTime() - date3.getTime();
                    Log.d(TAG, "DATA SELISIH: " + date_diff);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        time.start();
        try {
            time.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread() {
            String date;
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss.SSS");
            @Override
            public void run() {
                try {
                    while(true) {
                        date = dateFormat.format(TrueTime.now());

                        Date date2 = dateFormat.parse(date);
                        date2.getTime();

                        Date date3 = new Date();
                        date3.getTime();
                        date_diff = date2.getTime() - date3.getTime();
                        Thread.sleep(3000);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

        mhandler = new Handler();
        // startTimer();

        if (isMyServiceRunning(NotificationService.class)) {
            showDialogAlreadyRunning();
        } else {
            startMqtt();
            // heartraterepository.delete();

            checkBluetoothModule();
            Intent serviceIntent = new Intent(this, NotificationService.class);
            ContextCompat.startForegroundService(this, serviceIntent);

            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    setMessageSizeDialog();
                }
            });
        }
    }

    public void storeCarryForward(String c, String topic) {
        allStored = storedrepository.get();
        // if (mqttHelper.isConnected()) {
        if (isOnline()) {
//            for (int i = 0; i < allStored.size(); i++) {
//                mqttHelper.publishMsg(allStored.get(i).getStored(), topic);
//                // Log.d("scf", System.currentTimeMillis() + " published stored: " +
//                // allStored.get(i).getStored());
//                Log.d("scf", " published stored: " + allStored.get(i).getStored());
//            }
//            allStored.clear();

            mqttHelper.publishMsg(c, topic);
            // Log.d("scf", System.currentTimeMillis() + " published langsung: " + c);
            Log.d("scf", "published data");
            storedrepository.delete();
        } else {
            storedrepository.insert(new Stored(c));
            // Log.d("scf", System.currentTimeMillis() + " stored: " + c);
            Log.d("scf", " stored: " + c);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bluetoothAdapter.stopLeScan(scanCallback);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
    }

    private void scanBLe() {
        textHeartRate.setText("Scanning BLE Device");
        bluetoothAdapter.startLeScan(scanCallback);
        mhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                bluetoothAdapter.stopLeScan(scanCallback);
                if (myDevice != null) {
                    Log.d("ScanResult", "address Ticwatch S2 : " + myDevice.getAddress());
                    // Log.d("ScanResult", "address DUMMY : " + myDevice2.getAddress());
                    gatt = myDevice.connectGatt(getApplicationContext(), true, gattCallback, 2);
                    gatt.requestConnectionPriority(BluetoothGatt.CONNECTION_PRIORITY_HIGH);

                    Log.d("BLEConnection: ", "Initiating Connection...");
                } else {
                    showDialog();
                }
            }
        }, 2000);
    }

    int packetReceived = 1;
    private BluetoothAdapter.LeScanCallback scanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            // Log.i("test", "onLeScan: " + device.getAddress());
            if (device.getAddress().equals(MacAddress)) {
                myDevice = device;
                Log.d("packetReceived", packetReceived + " " + device.getAddress());
                packetReceived++;
            }
        }
    };

    private BluetoothGattCallback gattCallback = new BluetoothGattCallback() {

        // Untuk kebutuhan mendapatkan data characteristic 2
        List<BluetoothGattCharacteristic> characteristicList = new ArrayList<>();

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == STATE_CONNECTED) {
                Log.d("BLEConnection", myDevice.getAddress() + " connected");
                gatt.discoverServices();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {

            BluetoothGattCharacteristic characteristic = gatt
                    .getService(UUID.fromString("c86be7d3-12d4-4766-9c13-5d60cd5ee41e"))
                    .getCharacteristic(UUID.fromString("3d10750f-791d-48fb-af80-2219598e60f3"));

            gatt.setCharacteristicNotification(characteristic, true);

            BluetoothGattDescriptor descriptor = characteristic
                    .getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            gatt.writeDescriptor(descriptor);

            // Characteristic 2 berisi STEP
            BluetoothGattCharacteristic characteristic2 =
                    gatt.getService(UUID.fromString("c86be7d3-12d4-4766-9c13-5d60cd5ee41e"))
                            .getCharacteristic(UUID.fromString("365bdc33-e898-41f3-baf3-75d16f20bf2b"));

            gatt.setCharacteristicNotification(characteristic2, true);

            BluetoothGattDescriptor descriptor2 = characteristic2
                    .getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
            descriptor2.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            gatt.writeDescriptor(descriptor2);


            // Characteristic 3 berisi akselerasi
            BluetoothGattCharacteristic characteristic3 =
                    gatt.getService(UUID.fromString("c86be7d3-12d4-4766-9c13-5d60cd5ee41e"))
                            .getCharacteristic(UUID.fromString("3d435b53-955e-48ef-88ab-aae0b86a621f"));

            gatt.setCharacteristicNotification(characteristic3, true);

            BluetoothGattDescriptor descriptor3 = characteristic3
                    .getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
            descriptor3.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            gatt.writeDescriptor(descriptor3);

            // Characteristic 3 berisi akselerasi
            BluetoothGattCharacteristic characteristic4 =
                    gatt.getService(UUID.fromString("c86be7d3-12d4-4766-9c13-5d60cd5ee41e"))
                            .getCharacteristic(UUID.fromString("3f42bbff-a975-474c-b254-2aac532da7ad"));

            gatt.setCharacteristicNotification(characteristic4, true);

            BluetoothGattDescriptor descriptor4 = characteristic4
                    .getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
            descriptor3.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            gatt.writeDescriptor(descriptor4);

            characteristicList.add(characteristic4);
        }

        // kebutuhan characteristic 2
        public void requestCharacteristics(BluetoothGatt gatt) {
            gatt.readCharacteristic(characteristicList.get(0));
            //gatt.readDescriptor(descriptorList.get(0));
        }


        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            // Untuk kebutuhan characteristic 2
            // requestCharacteristics(gatt);
            // final byte[] data2 = characteristicList.get(0).getValue();

            // Ternyata, dua dua characteristic tanpa diminta get value nya, ngebroadcast ke satu characteristic
            Log.i(TAG, "onCharacteristicChanged: " + characteristic);
            final byte[] data = characteristic.getValue();

            // berhasil
            final ByteBuffer f2 = ByteBuffer.wrap(data);
            Log.d("TERIMA DATA", "onCharacteristicChanged: " + f2.capacity());

            // print the ByteBuffer
            // kalau gitu kita pakai logika pengecekan UUID dari characteristic yg masuk yaaa
            if (UUID.fromString("3d10750f-791d-48fb-af80-2219598e60f3").equals(characteristic.getUuid())) {
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        String timeVital = getDateTime(f2.getLong());
                        heartrate = f2.getFloat();
                        Log.d("ByteBufferHR", heartrate + " " + characteristic.getUuid());
                        // kebutuhan pengujian
                        int ID = f2.getInt();
                        String payload = timeVital + " | " + heartrate;
                        Log.d("DATA VITAL", timeVital + " menerima data ID " + ID + " waktu terima " + getCurrentTime());
                        storeCarryForward(payload + " | " + getCurrentTime() + " | ID " + ID, "sensor/heartrate");
                    }
                }; // jalankan thread
                thread.start();
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else if (UUID.fromString("365bdc33-e898-41f3-baf3-75d16f20bf2b").equals(characteristic.getUuid())) {
                Thread thread2 = new Thread() {
                    @Override
                    public void run() {
                        String timeStep = getDateTime(f2.getLong());

                        stepcounter = (int) f2.getFloat();
                        Log.d("ByteBufferStep", stepcounter + " " + characteristic.getUuid());
                        int ID = f2.getInt();
                        String payload = timeStep + " | " + stepcounter;
                        Log.d("DATA STEP", timeStep + " menerima data ID " + ID + " waktu terima " + getCurrentTime());
                        storeCarryForward(payload + " | " + getCurrentTime() + " | ID " + ID, "sensor/step");
                    }
                }; // jalankan thread
                thread2.start();
                try {
                    thread2.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else if (UUID.fromString("3d435b53-955e-48ef-88ab-aae0b86a621f").equals(characteristic.getUuid())) {
                Thread thread3 = new Thread() {
                    @Override
                    public void run() {
                        String timeActivity = getDateTime(f2.getLong());
                        int ID = 0;
                        for (int i = 1; i <= 3; i++) {
                            try {
                                if (i == 1) {
                                    acc_x = f2.getFloat();
                                    Log.d("ByteBufferAcc-X", acc_x + " " + characteristic.getUuid());
                                } else if (i == 2) {
                                    acc_y = f2.getFloat();
                                    Log.d("ByteBufferAcc-Y", acc_y + " " + characteristic.getUuid());
                                } else if (i == 3) {
                                    // data di komen dulu untuk kebutuhan pengujian
                                    // acc_z = f2.getFloat();
                                    // Log.d("ByteBufferAcc-Z", acc_z + " " + characteristic.getUuid());
                                    ID = f2.getInt();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        String payload = timeActivity + " | " + acc_x + "," + acc_y + "," + acc_z;
                        // storeCarryForward(payload + " | " + getCurrentTime(), "sensor/accelerometer");
                        // kebutuhan debug
                        Log.d("DATA ACC", timeActivity + " menerima data ID " + ID + " waktu terima " + getCurrentTime());
                        storeCarryForward(payload + " | " + getCurrentTime() + " | ID " + ID, "sensor/accelerometer");
                    }
                }; // jalankan thread
                thread3.start();
                try {
                    thread3.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else if (UUID.fromString("3f42bbff-a975-474c-b254-2aac532da7ad").equals(characteristic.getUuid())) {
                Thread thread4 = new Thread() {
                    @Override
                    public void run() {
                        String timeActivity = getDateTime(f2.getLong());
                        int ID = 0;
                        for (int i = 1; i <= 3; i++) {
                            try {
                                if (i == 1) {
                                    gyro_x = f2.getFloat();
                                    Log.d("ByteBufferGyro-X", gyro_x + " " + characteristic.getUuid());
                                } else if (i == 2) {
                                    gyro_y = f2.getFloat();
                                    Log.d("ByteBufferGyro-Y", gyro_y + " " + characteristic.getUuid());
                                } else {
                                    // data di komen dulu untuk kebutuhan pengujian
                                    // gyro_z = f2.getFloat();
                                    // Log.d("ByteBufferGyro-Z", gyro_z + " " + characteristic.getUuid());
                                    ID = f2.getInt();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        String payload = timeActivity + " | " + gyro_x + "," + gyro_y + "," + gyro_z;
                        // storeCarryForward(payload + " | " + getCurrentTime(), "sensor/gyroscope");
                        // kebutuhan debug
                        Log.d("DATA GYRO", timeActivity + " menerima data ID " + ID + " waktu terima " + getCurrentTime());
                        storeCarryForward(payload + " | " + getCurrentTime() + " | ID " + ID, "sensor/gyroscope");
                    }
                }; // jalankan thread
                thread4.start();
                try {
                    thread4.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            // di edit
            hrmdata.add(new heartrate(heartrate, stepcounter, acc_x, acc_y, acc_z));
            countDataTemp++;
//
            if (countDataTemp % messageSize == 0) {
                y = hrmdata.subList(index0, index1);

                index0 += messageSize;
                index1 += messageSize;
                message message = new message(a, y);
                a++;
                String json = gson.toJson(message);

                storeCarryForward(json + " | " + getCurrentTime(), "sensor");

                // mqttHelper.publishMsg(x.toString());
                Log.d("listSize", "allsize: " + hrmdata.size());
                if (countDataTemp == 1000) {
                    hrmdata.clear();
                    countDataTemp = 0;
                    index0 = 0;
                    index1 = messageSize;
                }
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    textHeartRate.setText(heartrate + " BPM");
                    textStepCounter.setText((int) stepcounter + " Langkah");
                    textAcceleration.setText("acc_x: " + acc_x + "\nacc_y: " + acc_y + "\nacc_z: " + acc_z);
                    textGyroscope.setText("gyro_x: " + gyro_x + "\ngyro_y: " + gyro_y + "\ngyro_z: " + gyro_z);
                }
//             if (mqttConnectStatus) {
//             text2.setText("Connected");
//             } else {
//             text2.setText("Disconnected");
//             }
//             text3.setText(String.valueOf(rawHeartRate));
//             }
            });
        }
    };

    private void startMqtt() {
        mqttHelper = new MqttHelper(getApplicationContext());
        mqttHelper.mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                Log.d("Debug", "Connected");
                mqttConnectStatus = true;
                Toast.makeText(getApplicationContext(), "connected to MQTT Broker", Toast.LENGTH_LONG).show();
            }

            @Override
            public void connectionLost(Throwable throwable) {
                Log.d("Debug", "Connection lost");
                mqttConnectStatus = false;
                Toast.makeText(getApplicationContext(), "Disconnected to MQTT Broker", Toast.LENGTH_LONG).show();
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {

            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            Log.d("InternetAvailable", "Internet Available");
            return true;
        } else {
            Log.d("InternetAvailable", "Internet Not Available");
            return false;
        }
    }

    private void showDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle("Node Sensor not found!");

        alertDialogBuilder.setMessage("Try Rescanning again?")
                // .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                scanBLe();
            }
        }).setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MainActivity.this.finish();
            }
        });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    private void showDialogAlreadyRunning() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle("Service is already running!");

        alertDialogBuilder.setMessage("Exit App?")
                // .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MainActivity.this.finish();
            }
        });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

    private void checkBluetoothModule() {
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);

            final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            bluetoothAdapter = bluetoothManager.getAdapter();
            scanBLe();
        } else {
            MainActivity.this.finish();
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void setMessageSizeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Change Message Size");

        final EditText input = new EditText(this);
        input.setText("200");

        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                messageSize = Integer.parseInt(input.getText().toString());
                Log.d("messageSize", String.valueOf(messageSize));
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

// public void startTimer() {
// //set a new Timer
// timer = new Timer();
//
// //initialize the TimerTask's job
// generateNoteOnSD(this, "batteryLevel");
//
// //schedule the timer, after the first 5000ms the TimerTask will run every
// 10000ms
// timer.schedule(timerTask, 0, 10000); //
// }
//
// public void generateNoteOnSD(final Context context, final String sFileName) {
// timerTask = new TimerTask() {
// public void run() {
//
// //use a handler to run a toast that shows the current timestamp
// mhandler2.post(new Runnable() {
// @RequiresApi(api = Build.VERSION_CODES.O)
// public void run() {
// if (ContextCompat.checkSelfPermission(getApplicationContext(),
// Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
// PackageManager.PERMISSION_GRANTED) {
// // Do the file write
// try {
// File root = new File(Environment.getExternalStorageDirectory(), "Notes");
//
// BatteryManager bm = (BatteryManager) getSystemService(BATTERY_SERVICE);
// batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
// FileOutputStream writer = new FileOutputStream(root + "/" + sFileName +
// ".txt", true);
// LocalTime time = LocalTime.now();
// writer.flush();
// writer.close();
// //Toast.makeText(context, "Saved on
// "+Environment.getExternalStorageDirectory(), Toast.LENGTH_SHORT).show();
// } catch (IOException e) {
// e.printStackTrace();
// }
// } else {
// // Request permission from the user
// ActivityCompat.requestPermissions(MainActivity.this,
// new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
// }
// }
// });
// }
// };
//
// }

    // kebutuhan debug
    public String getCurrentTime() {
        // SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss.SSS");
        String date;
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss.SSS");

//        date = dateFormat.format(TrueTime.now());

        /* use this for pengujian */
        Date date2 = new Date( new Date().getTime() + date_diff + 20 );
        date = dateFormat.format(date2);

        return date;
    }

    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }
    }

    // method convert getDateTime String
    private String getDateTime(long timeStamp) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());
        Date date = new Date(timeStamp);
        return dateFormat.format(date);
    }

}
