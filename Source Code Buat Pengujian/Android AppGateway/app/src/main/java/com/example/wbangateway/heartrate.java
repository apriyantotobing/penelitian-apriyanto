package com.example.wbangateway;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class heartrate {
    int stepcounter;
    float heartrate, acc_x, acc_y, acc_z;
    String time;

    public heartrate(float heartrate, int stepcounter, float acc_x, float acc_y, float acc_z){
        this.heartrate = heartrate;
        this.stepcounter = stepcounter;
        this.acc_x= acc_x;
        this.acc_y = acc_y;
        this.acc_z = acc_z;
    }

    // kebutuhan debug
    public static String getCurrentTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss.SSS");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }
}
