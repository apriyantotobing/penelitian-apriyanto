package com.example.wbangateway;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, NotificationService.class);
        context.stopService(serviceIntent);
        int id= android.os.Process.myPid();
        android.os.Process.killProcess(id);
    }
}
