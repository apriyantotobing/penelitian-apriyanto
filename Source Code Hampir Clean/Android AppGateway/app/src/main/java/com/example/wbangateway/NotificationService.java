package com.example.wbangateway;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import static android.app.Notification.EXTRA_NOTIFICATION_ID;
import static com.example.wbangateway.NotificationServiceChannel.CHANNEL_ID;
import static com.example.wbangateway.NotificationServiceChannel.CHANNEL_ID2;


public class NotificationService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Intent exit = new Intent(this, NotificationBroadcastReceiver.class);
        exit.setAction("test");
        exit.putExtra(EXTRA_NOTIFICATION_ID, 0);
        PendingIntent pendingExit =
                PendingIntent.getBroadcast(this, 0, exit, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Wban Gateway Service")
                .setContentText("Publishing heart rate data to cloud...")
                .setSmallIcon(R.drawable.ic_heart)
                .setContentIntent(contentPendingIntent)
                .addAction(R.drawable.ic_heart, "STOP SERVICE", pendingExit)
                .build();

        Notification notification2 = new NotificationCompat.Builder(this, CHANNEL_ID2)
                .setSmallIcon(R.drawable.ic_heart)
                .setContentTitle("Wban Gateway Service")
                .setContentText("Publishing heart rate data to cloud...")
                .setContentIntent(contentPendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .addAction(R.drawable.ic_heart, "STOP SERVICE", pendingExit)
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .build();

        startForeground(1, notification);
        startForeground(2,notification2);

        //do heavy work on a background thread
        //stopSelf();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
        stopSelf();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
