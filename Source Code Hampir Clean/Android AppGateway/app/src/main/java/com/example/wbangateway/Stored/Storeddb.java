package com.example.wbangateway.Stored;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Stored.class}, version = 2, exportSchema = false)
public abstract class Storeddb extends RoomDatabase {
    public abstract Storeddao daoAccess();
}