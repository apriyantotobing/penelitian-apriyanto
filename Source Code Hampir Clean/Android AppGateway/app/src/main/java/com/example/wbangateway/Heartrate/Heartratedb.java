package com.example.wbangateway.Heartrate;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Heartrate.class}, version = 2, exportSchema = false)
public abstract class Heartratedb extends RoomDatabase {
    public abstract Heartratedao daoAccess();
}