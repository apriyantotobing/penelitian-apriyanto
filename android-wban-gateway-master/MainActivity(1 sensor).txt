package com.example.wbangateway;

import android.app.Activity;
import android.app.AlertDialog;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import static android.bluetooth.BluetoothAdapter.STATE_CONNECTED;

public class MainActivity extends AppCompatActivity {

    private BluetoothAdapter bluetoothAdapter;
    private int REQUEST_ENABLE_BT;
    private String data = "Scanning BLE Device.....";
    private boolean mScanning = false;
    private boolean mqttConnectStatus;
    private boolean notFound;

    private TextView text1, text2, text3;
    private TextView dataReceived;
    private Button viewData;

    private Handler mhandler;
    private Handler mhandler2;

    private BluetoothGatt gatt;
    private BluetoothDevice myDevice, myDevice2;

    private int heartRate, dummyData;
    //private LiveData<List<HeartRate>> allHeartRates;
    private List<HeartRate> heartRates = new ArrayList<>();
    private List<Integer> asd = new ArrayList();
    private List qwe = new ArrayList();
    List x;

    MqttHelper mqttHelper;
    int countDataTemp = 0;
    int index0 = 0;
    int index1 = 200;
    int a = 1;

    Timer timer;
    TimerTask timerTask;

    HeartRateRepository heartRateRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text1 = (TextView) findViewById(R.id.HRMTextView);
        text2 = (TextView) findViewById(R.id.dataReceived);
        text3 = (TextView) findViewById(R.id.textViewDummy);
        text1.setText("connecting to AD8232...");
        text2.setText("MQTT Disconnected");
        text3.setText("connecting to Dummy...");

        heartRateRepository = new HeartRateRepository(getApplicationContext());
        //heartRateRepository.insertTask(77,true);
        heartRateRepository.deleteAllTask();

        viewData = (Button) findViewById(R.id.buttonViewData);
        viewData.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, view_heart_rate.class);
                startActivity(intent);

            }
        });

        mhandler = new Handler();
        mhandler2 = new Handler();

        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);

            final BluetoothManager bluetoothManager =
                    (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            bluetoothAdapter = bluetoothManager.getAdapter();

            scanBLe();
            startMqtt();
        } else {
            MainActivity.this.finish();
        }
    }


//                for (int j = 0; j < asd.size(); j++) {
//                        qwe.add(asd.get(j));
//                    }

    //System.out.println("size: "+qwe.size());
    //String json = new Gson().toJson(qwe);
    //System.out.println("size: "+qwe.size()+" "+json);
    //asd.clear();
    //qwe.clear();

    //}
    //Log.d("Data HRM-Sensor", String.format("Received heart rate: %d", heartRate));
    //use a handler to run a toast that shows the current timestamp
    public void storeCarryForward(String c) {

        heartRates = heartRateRepository.getBpmData();
        //MqttMessage message = new MqttMessage(c.toString().getBytes());
        if (isOnline()) {
            if (heartRateRepository.countData() > 0) {
                for (int i = 0; i < heartRates.size(); i++) {
                    Log.d("connected again" + i, String.valueOf(heartRates.get(i).getBpm()));
                    //MqttMessage storedMessage = new MqttMessage(String.valueOf(heartRates.get(i).getBpm()).getBytes());
                    mqttHelper.publishMsg(String.valueOf("stored " + heartRates.get(i).getBpm()));
                    Log.d("scf", "published stored: " + heartRates.get(i).getBpm());
                }
                heartRateRepository.deleteAllTask();
            } else {
                mqttHelper.publishMsg(c);
                Log.d("scf", "published langsung: " + c);
            }
        } else {
            Log.d("scf", "stored: " + c);
            heartRateRepository.insertTask(c);
        }

        //heartRateRepository.insertTask(heartRate,true);
        //Log.d("db_log", "run: saved to db");
        //Log.d("sent_message", String.format("payload: %d sent", heartRate));
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //bluetoothAdapter.stopLeScan(scanCallback);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bluetoothAdapter.stopLeScan(scanCallback);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
    }

    private void scanBLe() {
        text1.setText("Scanning BLE Device");
        bluetoothAdapter.startLeScan(scanCallback);
        mhandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                bluetoothAdapter.stopLeScan(scanCallback);
                if (myDevice != null) {
                    Log.d("ScanResult", "address AD8232 : " + myDevice.getAddress());
                    //Log.d("ScanResult", "address DUMMY  : " + myDevice2.getAddress());
                    gatt = myDevice.connectGatt(getApplicationContext(), true, gattCallback, 2);
                    //gatt = myDevice2.connectGatt(getApplicationContext(), true, gattCallback, 2);
                } else {
                    showDialog();
                }

            }
        }, 3000);
    }

    private BluetoothAdapter.LeScanCallback scanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                    if (device.getAddress().equals("30:AE:A4:05:46:B6")) {
                        myDevice = device;
                        Log.d("bleAddress", "device1 address :" + myDevice.getAddress());
                    }

//                    if(device.getAddress().equals("30:AE:A4:40:DF:6E")){
//                        myDevice2 = device;
//                        Log.d("bleAddress","device2 address :"+myDevice2.getAddress());
//                    }

                }


            };

    private BluetoothGattCallback gattCallback =
            new BluetoothGattCallback() {
                public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                    if (newState == STATE_CONNECTED) {
                        Log.d("connection", "onConnectionStateChange: connected");
                        gatt.discoverServices();
                    }
                }

                public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                    Log.d("on service discovered", "onServicesDiscovered: discovered");

                    if (gatt.getDevice().getAddress().equals("30:AE:A4:05:46:B6")) {
                        BluetoothGattCharacteristic characteristic =
                                gatt.getService(UUID.fromString("0000180d-0000-1000-8000-00805f9b34fb"))
                                        .getCharacteristic(UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb"));

                        gatt.setCharacteristicNotification(characteristic, true);

                        BluetoothGattDescriptor descriptor =
                                characteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));

                        descriptor.setValue(
                                BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                        gatt.writeDescriptor(descriptor);
                    }

//                    if(gatt.getDevice().getAddress().equals("30:AE:A4:40:DF:6E")){
//                        BluetoothGattCharacteristic characteristic =
//                                gatt.getService(UUID.fromString("4fafc201-1fb5-459e-8fcc-c5c9c331914b"))
//                                        .getCharacteristic(UUID.fromString("beb5483e-36e1-4688-b7f5-ea07361b26a8"));
//
//                        gatt.setCharacteristicNotification(characteristic, true);
//
//                        BluetoothGattDescriptor descriptor =
//                                characteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
//
//                        descriptor.setValue(
//                                BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
//                        gatt.writeDescriptor(descriptor);
//                    }
                }

                public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
                    Log.d("on decriptor writer", "onDescriptorWrite: done");

                    if (gatt.getDevice().getAddress().equals("30:AE:A4:05:46:B6")) {
                        BluetoothGattCharacteristic characteristic =
                                gatt.getService(UUID.fromString("0000180d-0000-1000-8000-00805f9b34fb"))
                                        .getCharacteristic(UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb"));
                        characteristic.setValue(new byte[]{1, 1});
                        gatt.writeCharacteristic(characteristic);
                    }

//                    if(gatt.getDevice().getAddress().equals("30:AE:A4:40:DF:6E")){
//                        BluetoothGattCharacteristic characteristic =
//                                gatt.getService(UUID.fromString("4fafc201-1fb5-459e-8fcc-c5c9c331914b"))
//                                        .getCharacteristic(UUID.fromString("beb5483e-36e1-4688-b7f5-ea07361b26a8"));
//                        characteristic.setValue(new byte[]{1, 1});
//                        gatt.writeCharacteristic(characteristic);
//                    }
                }

                public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
                    //data = characteristic.getStringValue(0);

                    if (gatt.getDevice().getAddress().equals("30:AE:A4:05:46:B6")) {
                        int flag = characteristic.getProperties();
                        int format = -1;
                        if ((flag & 0x01) != 0) {
                            format = BluetoothGattCharacteristic.FORMAT_UINT16;
                            //Log.d("HRM Data Format", "Heart rate format UINT16.");
                        } else {
                            format = BluetoothGattCharacteristic.FORMAT_UINT8;
                            //Log.d("HRM Data Format", "Heart rate format UINT8.");
                        }
                        heartRate = characteristic.getIntValue(format, 1);

                        asd.add(a);
                        a++;
                        countDataTemp++;
                        //System.out.println(countDataTemp);
                        if (countDataTemp % 200 == 0) {
                            x = asd.subList(index0, index1);
                            //System.out.println("listSize: " + x.size() + " " + x);
                            index0 += 200;
                            index1 += 200;
                            storeCarryForward(x.toString());
                            //mqttHelper.publishMsg(x.toString());
                            countDataTemp = 0;
                        }
                    }

//                    if(gatt.getDevice().getAddress().equals("30:AE:A4:40:DF:6E")){
//                        int flag = characteristic.getProperties();
//                        int format = -1;
//                        if ((flag & 0x01) != 0) {
//                            format = BluetoothGattCharacteristic.FORMAT_UINT16;
//                            //Log.d("HRM Data Format", "Heart rate format UINT16.");
//                        } else {
//                            format = BluetoothGattCharacteristic.FORMAT_UINT8;
//                            //Log.d("HRM Data Format", "Heart rate format UINT8.");
//                        }
//                        dummyData = characteristic.getIntValue(format, 1);
//                    }
                    //Log.d("Data HRM-Sensor", String.format("Received heart rate: %d", heartRate));


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            text1 = (TextView) findViewById(R.id.HRMTextView);
                            text3 = (TextView) findViewById(R.id.textViewDummy);

                            text1.setText(String.valueOf(heartRate) + " BPM");
                            text2.setText("Connected");
                            text3.setText(String.valueOf(dummyData));
                        }
                    });
                }
            };

    private void startMqtt() {
        mqttHelper = new MqttHelper(getApplicationContext());
        mqttHelper.mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                mqttConnectStatus = true;
                Log.w("Debug", "Connected");
            }

            @Override
            public void connectionLost(Throwable throwable) {
                Log.w("Debug", "Connection lost");
                mqttConnectStatus = false;
                //heartRateRepository.insertTask(heartRate,true);
                //Log.w("stored",String.valueOf(heartRate));
            }

            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                //Log.w("Debug",mqttMessage.toString());
//                final HeartRateRepository heartRateRepository = new HeartRateRepository(getApplicationContext());
//                heartRateRepository.insertTask(Integer.parseInt(mqttMessage.toString()),true);

//                heartRateRepository.getTasks().observe(MainActivity.this, new Observer<List<HeartRate>>() {
//                    @Override
//                    public void onChanged(@Nullable List<HeartRate> heartRates) {
//                        for(HeartRate heartRate : heartRates) {
//                            //Log.d("test", String.valueOf(heartRate.getId())+" "+String.valueOf(heartRate.getBpm()));
//                        }
//                    }
//                });
                //dataReceived.setText(mqttMessage.toString());
                //mChart.addEntry(Float.valueOf(mqttMessage.toString()));
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    private void showDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        alertDialogBuilder.setTitle("Node Sensor not found!");

        alertDialogBuilder
                .setMessage("Try Rescanning again?")
                //.setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        scanBLe();
                    }
                })
                .setNegativeButton("Exit App", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        MainActivity.this.finish();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

}
