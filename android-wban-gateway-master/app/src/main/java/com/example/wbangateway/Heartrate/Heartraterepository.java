package com.example.wbangateway.Heartrate;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

public class Heartraterepository {

    private String DB_NAME = "db_task";

    private Heartratedb heartRateDatabase;

    public Heartraterepository(Context context) {
        heartRateDatabase = Room.databaseBuilder(context, Heartratedb.class, DB_NAME).allowMainThreadQueries().fallbackToDestructiveMigration().build();
    }

    public void insert(final Heartrate heartRate) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                heartRateDatabase.daoAccess().insert(heartRate);
                return null;
            }
        }.execute();
    }

    public void delete() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                heartRateDatabase.daoAccess().delete();
                return null;
            }
        }.execute();
    }

    public LiveData<List<Heartrate>> getLive() {
        return heartRateDatabase.daoAccess().getLive();
    }

    public List<Heartrate> get(){
        return heartRateDatabase.daoAccess().get();
    }

    public int count(){
        return heartRateDatabase.daoAccess().count();
    }
}