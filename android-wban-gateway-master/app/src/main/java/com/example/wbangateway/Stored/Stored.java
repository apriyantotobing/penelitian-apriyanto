package com.example.wbangateway.Stored;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "stored")
public class Stored {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;
    String stored;

    public Stored(String stored) {
        this.stored = stored;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStored() {
        return stored;
    }

    public void setStored(String stored) {
        this.stored = stored;
    }
}
