package com.example.wbangateway.Heartrate;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Heartrate.class}, version = 2)
public abstract class Heartratedb extends RoomDatabase {
    public abstract Heartratedao daoAccess();
}