package com.example.wbangateway.Heartrate;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "heartrate")
public class Heartrate {
    @PrimaryKey
    @NonNull
    private int id;
    int raw;
    int bpm;

    public Heartrate(int id, int raw, int bpm) {
        this.id = id;
        this.raw = raw;
        this.bpm = bpm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRaw() {
        return raw;
    }

    public void setRaw(int raw) {
        this.raw = raw;
    }

    public int getBpm() {
        return bpm;
    }

    public void setBpm(int bpm) {
        this.bpm = bpm;
    }
}
