package com.example.wbangateway.Stored;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface Storeddao {

    @Insert
    void insert(Stored stored);

    @Query("SELECT * FROM stored")
    LiveData<List<Stored>> getLive();

    @Query("SELECT * FROM stored")
    List<Stored> get();

    @Query("DELETE FROM stored")
    void delete();

}
