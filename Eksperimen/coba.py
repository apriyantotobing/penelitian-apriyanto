import time
import paho.mqtt.client as mqtt
# Personalisasi timestamp
from datetime import datetime
# kebutuhan logging data
import json
from regex import selisih2


count = 1

# Callback Function on Connection with MQTT Server
def on_connect( client, userdata, flags, rc):
    if rc == 0:
        print("Connected to broker")
    else:
        print("Connection failed")
    # Subscribe Topic from here
    # client.subscribe("sensor/#")
    client.subscribe("sensor/heartrate") # data.txt
    # client.subscribe("sensor/step") # data2.txt
    # client.subscribe("sensor/accelerometer") # data3.txt
    # client.subscribe("sensor/gyroscope") # data4.txt

# Callback Function on Receiving the Subscribed Topic/Message
def on_message( client, userdata, msg):
    # waktu yang di print : tanggal | jam-menit-detik-microdetik
    dateTimeObj = datetime.now()
    timestampStr = dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S.%f)")
    
    
    data = "[" + msg.payload.decode('utf-8') + "]"
    # print(data)
    # outfile.writelines("%s\n" % (line + " | " + timestampStr) for line in data[0]["heartrates"])        
    st = "{}\n".format(msg.payload.decode('utf-8') + " | TERIMA " + timestampStr)
    
    # saya mau simpan string json tersebut ke dalam file text
    with open('data.txt', 'a') as outfile:
        outfile.writelines(st)

    count+=1
    # bakal ada 3 timestamp yang diterima
    # pertama adalah data dari sensor 
    # kedua adalah data dari gateway
    # ketiga adalah data dari laptop saat subscribe
    print("Data ke {} ".format(count) + selisih2(st, 1, 2))


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message


broker_address= "tailor.cloudmqtt.com"  #Broker address
port = 16503                         #Broker port
user = "lvntsnrq"                    #Connection username
password = "79W8iNWE4G9i"            #Connection password

client.username_pw_set(user, password)
client.connect(broker_address, port, 60)

client.loop_forever()
# client.loop_start()
time.sleep(1)


# client.loop_stop()
# client.disconnect()
# 165150201111221
# Shafira Puspa Fitrotuzzakiyah

# 165150200111184
# Apriyanto JPL Tobing