import re
import datetime
from datetime import timedelta

# from itertools import izip


def selisih(st, first, last):
    # ini bakal ambil data tanggal dari data.txt
    the_regex = re.compile(r'\d{2}:\d{2}:\d{2}.\d{3}')
    string_to_search_in = st
    matches = re.findall(the_regex, string_to_search_in)
    # return dalam list

    datetimeFormat = '%H:%M:%S.%f'
    # date1 itu waktu pengirim
    date1 = matches[first] # 0
    # date2 itu waktu penerima
    date2 = matches[last] # 1
    diff = datetime.datetime.strptime(
        date2, datetimeFormat) - datetime.datetime.strptime(date1, datetimeFormat)

    print("Delay Milisecond:", (diff.days * 24 * 60 * 60 + diff.seconds)
        * 1000 + diff.microseconds / 1000, "\n-------------------")


def selisih2(st, first, last):
    # ini bakal ambil data tanggal dari data.txt
    the_regex = re.compile(r'\d{2}:\d{2}:\d{2}.\d{3}')
    string_to_search_in = st
    matches = re.findall(the_regex, string_to_search_in)
    # return dalam list

    datetimeFormat = '%H:%M:%S.%f'
    # date1 itu waktu pengirim
    date1 = matches[first] # 0
    # date2 itu waktu penerima
    date2 = matches[last] # 1
    diff = datetime.datetime.strptime(
        date2, datetimeFormat) - datetime.datetime.strptime(date1, datetimeFormat)

    return"{}".format((diff.days * 24 * 60 * 60 + diff.seconds) * 1000 + diff.microseconds / 1000)

# berhasil
def deteksi(dataSensor, dataGateway):
    count2 = 1
    delay = 0
    maxdelay = 0
    mindelay = 10000000
    print("Delay dari {} terhadap {}".format(dataSensor, dataGateway))
    with open("{}.txt".format(dataGateway)) as textfile1:
        for line in textfile1.readlines():
            match = re.search(r'ID (\S+)', line)
            
            if match is not None:
                data = match.group(1)
            with open("{}.txt".format(dataSensor)) as textfile2:
                for num, line2 in enumerate(textfile2, 1):
                    pattern = str(data)
                    the_regex = re.compile(r"(?:^|\W)ID " + re.escape(pattern) + r"(?:$|\W)")
                    
                    if the_regex.findall(line2):
                        # print(str(count2) + " Data ada! ID: " + data + " baris ke " + str(num) + " Delay " +
                        # selisih2(line, 1, 0))
                        count2+=1
                        delay+=float(selisih2(line, 1, 0))
                        # gantinya ini yaa untuk yg store forward : selisih2(line2 + line, 0, 1)
                        if maxdelay < float(selisih2(line, 1, 0)):
                            maxdelay = float(selisih2(line, 1, 0))
                        if float(selisih2(line, 1, 0)) < mindelay:
                            mindelay = float(selisih2(line, 1, 0))
                        # data dari sensor - gateway
                        # st = selisih2(line2 + line, 0, 1)
                        # with open('dataHasil.txt', 'a') as outfile:
                        #     outfile.writelines(str(st))
                        break
            
        print("Rata-rata delay {}".format(str(delay/(count2 - 1))))
        print("Maksimal delay {}".format(maxdelay))
        print("Minimum delay {}".format(mindelay))


def deteksi2(dataGateway, dataCloud):
    count2 = 1
    delay = 0
    maxdelay = 0
    mindelay = 10000000
    print("Delay dari {} terhadap {}".format(dataGateway, dataCloud))
    with open("{}.txt".format(dataCloud)) as textfile1:
        for line in textfile1.readlines():
            match = re.search(r'ID (\S+)', line)
            if match is not None:
                data = match.group(1)
            with open("{}.txt".format(dataGateway)) as textfile2:
                for num, line2 in enumerate(textfile2, 1):
                    pattern = str(data)
                    the_regex = re.compile(r"(?:^|\W)ID " + re.escape(pattern) + r"(?:$|\W)")
                    
                    if the_regex.findall(line2):
                        print(str(count2) + " Data ada! ID: " + data + " baris ke " + str(num) + " Delay " +
                        selisih2(line, 1, 2))
                        count2+=1
                        if maxdelay < float(selisih2(line, 1, 2)):
                            maxdelay = float(selisih2(line, 1, 2))
                        if float(selisih2(line, 1, 2)) < mindelay:
                            mindelay = float(selisih2(line, 1, 2))
                        # bakal ada 3 timestamp yang diterima
                        # pertama adalah data waktu dari sensor 
                        # kedua adalah data dari gateway
                        # ketiga adalah data dari laptop saat subscribe
                        delay+=float(selisih2(line, 1, 2))
                        break
            
        print("Rata-rata delay {}".format(str(delay/(count2 - 1))))
        print("Maksimal delay {}".format(maxdelay))
        print("Minimum delay {}".format(mindelay))


def truefalse(dataAsal, dataAkhir):
    count2 = 0
    isData = False
    with open("{}.txt".format(dataAsal)) as textfile1:
        for line in textfile1.readlines():
            match = re.search(r'ID (\S+)', line)
            if match is not None:
                data = match.group(1)
                count2+=1
                
            with open("{}.txt".format(dataAkhir)) as textfile2:
                for num, line2 in enumerate(textfile2, 1):
                    pattern = str(data)
                    the_regex = re.compile(r"(?:^|\W)ID " + re.escape(pattern) + r"(?:$|\W)")
                    if the_regex.findall(line2):
                        with open('{}.txt'.format("selesele"), 'a') as outfile:
                            # outfile.writelines("TRUE\n")
                            outfile.writelines(selisih2((line + line2), 1, 2) + "\n")
                        isData = True
                        break
                    else:
                        isData = False
                        continue
            if not isData:
                with open('{}.txt'.format("selesele"), 'a') as outfile:
                    # outfile.writelines("FALSE\n")
                    outfile.writelines('0\n')
                

# berhasil
def pisah(namafile, pattern, namafiletujuan):
    with open("{}.txt".format(namafile)) as textfile1:
        for line in textfile1.readlines():
            the_regex = re.compile(r"(?:^|\W)" + re.escape(pattern) + r"(?:$|\W)")
            if the_regex.findall(line):
                with open('{}.txt'.format(namafiletujuan), 'a') as outfile:
                    outfile.writelines(line)

# x="123"
# y="123"

# pattern = input()
# the_regex = re.compile(r"(?:^|\W)" + re.escape(pattern) + r"(?:$|\W)")
# print(the_regex.findall(y))
# print(the_regex.findall(x))



# catatan:
# data = adalah data dari cloud topik heartrate
# data2 = Step
# data3 = adalah accelerometer
# data4 = adalah gyroscope

# BLEsensor menyimpan data semua log dari sensor saat pengiriman melalui BLE
# BLEgateway menyimpan data semua log dari gateway saat pengiriman melalui BLE

# data kemudian dipisah berdasarkan tag nya dengan fungsi pisah
# dataHeartSensor = adalah data dari node sensor heartrate
# dataHeartGateway = adalah data dari node gateway heartrate
# dataStepSensor = adalah data dari node sensor step
# dataStepGateway = adalah data dari node gateway step
# dataAccSensor = adalah data dari node sensor accelerometer
# dataAccGateway = adalah data dari node gateway accelerometer
# dataGyroSensor = adalah data dari node sensor gyroscope
# dataGyroGateway = adalah data dari node gateway gyroscope

# kemudian lakukan perbandingan dan selisih dengan menggunakan method sebagai berikut
# bandingkan timestamp dari gateway BLE dengan sensor BLE berdasarkan masing-masing kelompok data

# bandingkan timestamp dari cloud MQTT dengan gateway MQTT berdasarkan kelompok data

# jadi urutannya
# def pisah () -> BLEgateway, ACC / GYRO / VITAL / STEP
# def pisah () -> BLEsensor, ACC / GYRO / VITAL / STEP

# def deteksi () -> dataSensor ACC / GYRO / VITAL / STEP, dataGateway ACC / GYRO / VITAL / STEP
# def deteksi2 () -> dataGateway ACC / GYRO / VITAL / STEP, dataCloud ACC / GYRO / VITAL / STEP


# proses pemisahan
# namafile = input()
# pattern = input()
# namafiletujuan = input()
# pisah(namafile, pattern, namafiletujuan)

namafile1 = "BLEsensor"
namafile2 = "BLEgateway"
pattern1 = "VITAL"
pattern2 = "STEP"
pattern3 = "ACC"
pattern4 = "GYRO"
namafiletujuan1 = "dataHeartSensor"
namafiletujuan2 = "dataHeartGateway"
namafiletujuan3 = "dataStepSensor"
namafiletujuan4 = "dataStepGateway"
namafiletujuan5 = "dataAccSensor"
namafiletujuan6 = "dataAccGateway"
namafiletujuan7 = "dataGyroSensor"
namafiletujuan8 = "dataGyroGateway"

cloud1 = "data"
cloud2 = "data2"
cloud3 = "data3"
cloud4 = "data4"

# pisah(namafile1, pattern1, namafiletujuan1)
# pisah(namafile2, pattern1, namafiletujuan2)
# pisah(namafile1, pattern2, namafiletujuan3)
# pisah(namafile2, pattern2, namafiletujuan4)
# pisah(namafile1, pattern3, namafiletujuan5)
# pisah(namafile2, pattern3, namafiletujuan6)
# pisah(namafile1, pattern4, namafiletujuan7)
# pisah(namafile2, pattern4, namafiletujuan8)

# dataSensor = input()
# dataGateway = input()
# deteksi(dataSensor, dataGateway)
# kalau bisa hasil deteksi ini di write ke suatu file biar bagus

# dataSensor = namafiletujuan5
# dataGateway = namafiletujuan6
# deteksi(dataSensor, dataGateway)


dataSensor = namafiletujuan1
dataGateway = namafiletujuan2
deteksi(dataSensor, dataGateway)


# dataGateway = input()
# dataCloud = input()
# deteksi2(dataGateway, dataCloud)

# dataGateway = namafiletujuan2
# dataCloud = cloud1
# deteksi2(dataGateway, dataCloud)


# dataAsal = namafiletujuan5
# dataAkhir = cloud3
# truefalse(dataAsal, dataAkhir)

# dataAsal = namafiletujuan3
# dataAkhir = cloud2
# truefalse(dataAsal, dataAkhir)

# dataAsal = namafiletujuan5
# dataAkhir = cloud3
# truefalse(dataAsal, dataAkhir)

# dataAsal = namafiletujuan5
# dataAkhir = namafiletujuan6
# truefalse(dataAkhir, dataAsal)

# dataAsal = namafiletujuan7
# dataAkhir = cloud4
# truefalse(dataAsal, dataAkhir)